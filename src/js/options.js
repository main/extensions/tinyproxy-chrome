/* global CodeMirror */

jQuery(() => {

	let editor;

	$("#options_form").on("submit", () => {

		const settings = {};

		$.each($("#options_form").serializeArray(), (i, a) => {
			settings[a.name] = a.value;
		});

		settings['whitelist'] = editor.getValue();

		chrome.storage.sync.set({'settings' : settings}, () => {
			console.log("settings saved");
			chrome.runtime.sendMessage({method: "ping"});
		});

		$("#message").html("Data saved.");

		return false;
	});

	chrome.storage.sync.get('settings', (ret) => {
		if (typeof ret.settings !== "undefined") {
			$.each(ret.settings, (k,v) => {
				$("#" + k).val(v);
			});
		}

		$("#whitelist").attr("placeholder", "example.com - exact match\n.example.com - include subdomains");

		editor = CodeMirror.fromTextArea($("#whitelist")[0],
					{ lineNumbers: 1 });
	});

});
