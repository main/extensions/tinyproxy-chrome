function generate_pac(settings) {
	try {
		const urls = settings.whitelist.split("\n");
		const i2p_proxy_url = settings.i2p_url;
		const socks_proxy_url = settings.socks_proxy_url;

		let proxy_url;
		let proxy_proto;

		if (socks_proxy_url) {
			proxy_url = socks_proxy_url;
			proxy_proto = "SOCKS5";
		} else {
			proxy_url = settings.url;
			proxy_proto = "HTTPS";
		}

		let pac_rules = '';

		if (i2p_proxy_url)
			pac_rules += `if (dnsDomainIs(host, ".i2p")) { return 'PROXY ${i2p_proxy_url}'; }\n`;

		for (let i = 0; i < urls.length; i++) {
			const u = urls[i].replace("'", "\\'");

			if (u) {
				if (u[0] == ".") {
					pac_rules += ` if (dnsDomainIs(host, '${u.substring(1)}')) { return '${proxy_proto} ${proxy_url}'; }\n`;
				} else {
					pac_rules += ` if (host == '${u}') { return '${proxy_proto} ${proxy_url}'; }\n`;
				}
			}
		}

		const pac_script = `
			function FindProxyForURL(url, host) {
				if (isPlainHostName(host)) { return 'DIRECT'; }
				if (host == '${proxy_url.replace(/:.*/, "")}') { return 'DIRECT'; }
				${pac_rules}
				return 'DIRECT';
			}
		`;

		console.log('generated pac', pac_script);

		return pac_script;

	} catch (e) {
		console.warn(e);
		return false;
	}
}

function set_proxy(settings, reachable) {

	if (!reachable) {
		console.log("proxy set as unreachable, removing...");

		if (chrome.proxy.settings) {
			chrome.proxy.settings.set(
				{value: {mode: "direct"}, scope: 'regular'}, () => { console.log("proxy removed"); });
		}

		return;
	} else {
		console.log("reachable, setting proxy...");
	}

	const pac = generate_pac(settings);

	if (!pac) return;

	const config = {
		mode: "pac_script",
		pacScript: { mandatory: true, data: pac }
	}

	chrome.proxy.settings.set(
		{value: config, scope: 'regular'}, () => { console.log("proxy set"); });
}

function set_title(title) {
	chrome.action.setTitle({title: title});
}

function set_badge(text, color) {
	chrome.action.setBadgeText({text: text});

	if (color)
		chrome.action.setBadgeBackgroundColor({color: color});
}

function ping() {
	console.log('ping invoked');

	chrome.storage.sync.get('settings', (ret) => {
		try {
			const settings = ret.settings;
			const ping_url = 'https://' + settings.url.split(/:/)[0] + '/ping';

			console.log('ping', 'ping_url', ping_url, 'settings', settings);

			fetch(ping_url, {
				method: 'POST',
				credentials: 'omit',
				headers: {
					'Authorization': 'Basic ' + btoa(settings.login + ':' + settings.password),
					}
				}
			)
			.then((resp) => {
				console.log('ping', 'got http status', resp.status);

				if (resp.status == 200) {
					set_badge('ok', 'green');
					set_title('Proxy enabled');

					set_proxy(settings, true);

				} else if (resp.status == 401) {
					set_badge('er5', 'red');
					set_title('Ping refused, check your credentials');

					set_proxy(settings, false);

				} else {
					set_badge('er4', 'red');
					set_title('Ping request failed');

					set_proxy(settings, false);
				}
			})
			.catch((err) => {
				set_badge('er3', 'red');
				set_title(`Ping request failed: ${err}`);

				set_proxy(settings, false);
			});

		} catch (e) {
			console.warn(e);

			set_badge('err', 'red');
			set_title("Configuration error");
			set_proxy(ret.settings, false);
		}
	});
}

function auth_callback(details, callbackFn) {

	console.log('auth_callback', details);

	chrome.storage.sync.get('settings', (ret) => {
		const settings = ret.settings;

		if (settings) {
			if (settings.login && settings.password) {

				if (details.isProxy === !0)
					callbackFn({authCredentials: {
						username: settings.login,
						password: settings.password}
					});
				else
					callbackFn({});
			}
		}
	});
}

chrome.webRequest.onAuthRequired.addListener(
	auth_callback,
	{urls: ["<all_urls>"]},
	['asyncBlocking'] );

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
   if (request.method == "ping") {
		ping();
	}
});

chrome.action.onClicked.addListener(() => {
	ping();
});

chrome.alarms.onAlarm.addListener((/*alarm*/) => {
	ping();
});

function init() {
   chrome.alarms.get('alarm', (alarm) => {
      if (!alarm)
         chrome.alarms.create('alarm', {periodInMinutes: 5});
   });

	ping();
}

chrome.runtime.onInstalled.addListener(() => {
	console.log('onInstalled');
	init();
});

chrome.runtime.onStartup.addListener(() => {
	console.log('onStartup');
	init();
});
