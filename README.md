# Selective HTTPS proxy for Chrome/Firefox

With optional support for:

- Using I2P proxy for .i2p domains (note that connection to I2P proxy is not encrypted).
- Using SOCKS proxy instead of HTTPS

Ensures connectivity for otherwise blocked websites by routing user-specified domains (optionally including subdomains) via a secure proxy.

Typically used with squid (ssl + basic auth) and nginx (should respond to ``/ping`` on ``proxy host:443`` if proxy credentials are correct) - ``/ping`` is used to check connectivity.

You can use a different proxy and/or https server as long as it responds to the extension as outlined below.

# [Dumbproxy](https://github.com/Snawoot/dumbproxy) example

```yml
version: '3'
services:
dumbproxy:
   image: yarmak/dumbproxy
   restart: unless-stopped
   network_mode: host
   container_name: dumbproxy
   cap_drop:
      - ALL
   command: >
      -bind-address :8443
      -cert /certs/ca.pem
      -key /certs/ca.key
      -auth basicfile://?path=/users
   volumes:
      - /opt/dumbproxy/certs:/certs:ro
      - /opt/dumbproxy.users:/users:ro # usual htpasswd format
   ports:
      - 8443:8443/tcp
```

# Squid example (deprecated)

```
sslproxy_cipher EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA+RC4:EECDH:EDH+aRSA:HIGH:!RC4:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS

https_port 8443 generate-host-certificates=on dynamic_cert_mem_cache_size=4MB cert=... key=... dhparams=... options=NO_SSLv3

auth_param basic program /usr/lib/squid3/basic_ncsa_auth /etc/squid/users
auth_param basic realm proxy

...

acl authenticated proxy_auth REQUIRED

http_access deny CONNECT !SSL_ports
http_access deny !authenticated
http_access allow authenticated
http_access deny all
```

Your distro might ship Squid built without SSL support. In this case, you will need to rebuild it or obtain a different pre-built package.

# Nginx example (for ping)

```nginx
location /ping {
   auth_basic sample;
   auth_basic_user_file /opt/dumbproxy.users; # same htpasswd file as above

   try_files DUMMY @return200;
}

location @return200 {
   return 200 'pong';
}
```

# License

GPLv3.
